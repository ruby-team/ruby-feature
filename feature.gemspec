#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: feature 1.4.0 ruby lib

Gem::Specification.new do |s|
  s.name = "feature".freeze
  s.version = "1.4.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Markus Gerdes".freeze]
  s.date = "2016-06-11"
  s.email = "github@mgsnova.de".freeze
  s.files = ["CHANGELOG.md".freeze, "Gemfile".freeze, "README.md".freeze, "Rakefile".freeze, "lib/feature.rb".freeze, "lib/feature/generators/install_generator.rb".freeze, "lib/feature/generators/templates/feature.rb".freeze, "lib/feature/repository.rb".freeze, "lib/feature/repository/active_record_repository.rb".freeze, "lib/feature/repository/redis_repository.rb".freeze, "lib/feature/repository/simple_repository.rb".freeze, "lib/feature/repository/yaml_repository.rb".freeze, "lib/feature/testing.rb".freeze, "spec/feature/active_record_repository_spec.rb".freeze, "spec/feature/feature_spec.rb".freeze, "spec/feature/redis_repository_spec.rb".freeze, "spec/feature/simple_repository_spec.rb".freeze, "spec/feature/testing_spec.rb".freeze, "spec/feature/yaml_repository_spec.rb".freeze, "spec/integration/rails/gemfiles/rails4.gemfile".freeze, "spec/integration/rails/test-against-several-rails-versions.sh".freeze, "spec/integration/rails/test-against-specific-rails-version.sh".freeze, "spec/spec_helper.rb".freeze]
  s.homepage = "http://github.com/mgsnova/feature".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.5.2.1".freeze
  s.summary = "Feature Toggle library for ruby".freeze
end
